# Copyright (c) 2013, Indictrans and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns = get_column()
	data = get_data(filters)
	return columns, data
	
def get_column():
	return [_("Visitor No")+ ":Link/Visitor Information:150",
		 _("Visitor Name") + ":Data:150",_("Contact No") + ":Data:100",
		 _("Department") + ":Data:100",
		 _("Location") + ":Data:100",_("Vehicle Information") + ":Data:100",
		 _("Date of Visit") + ":Date:100",_("Check In Time") + ":Time:100",
		 _("Check Out Time") + ":Time:100",
		 _("Vistor Type") + ":Data:90",_("Purpose of Visit") + ":Data:100",
		 _("Laptop Serial No") + ":Data:90", _("Pendrive") + ":check:70",
		 _("Phones") + ":check:70",_("Mobile Information") + ":Data:100",
		 _("Hard Disk") + ":check:70",_("To Visit") + ":Data:100",
		 _("Organisation") + ":Data:100",_("Emp Department") + ":Data:100",
		 _("Designation") + ":Data:100",
	]

def condition(filters):

	user = frappe.session.user
	condition =""

	if user=="Administrator":
		if filters.get('visitor_type') != 'All':
			condition += "where vistor_type='{0}'".format(filters.get('visitor_type'))	
		return condition

	else:
		if filters.get('visitor_type') != 'All' :
	 		condition += " where owner='{}' and vistor_type='{}'".format(user, filters.get('visitor_type'))
		else:
			condition += " where owner='{}'".format(user)
		return condition
	

def get_data(filters):
	query = """
			Select 
				visitor_no, visitor_name, contact_no,
				department, location, vehicle_information,
				date_of_visit, check_in_time, check_out_time, vistor_type, 
				purpose_of_visit, serial_number, pendrive, phones,
				mobile_information, hard_disk, to_visit, 
				organisation, emp_department, designation 
			from 
				`tabVisitor Information` {} order by visitor_no DESC
			""" .format(condition(filters))
	return frappe.db.sql(query, as_list=1)