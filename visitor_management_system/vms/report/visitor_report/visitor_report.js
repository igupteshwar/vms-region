// Copyright (c) 2016, Indictrans and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Visitor Report"] = {
	"filters": [
		// {
		// 	"fieldname":"select_month",
		// 	"label": __("Select Month"),
		// 	"fieldtype": "Select",
		// 	"options": "Jan\nFeb\nMar\nApr",
		// 	"default": "Jan"
		// },
		// {
		// 	"fieldname":"fiscal_yea",
		// 	"label": __("Fiscal Yea"),
		// 	"fieldtype": "Link",
		// 	"options": "Fiscal Year"
		// },
		{
			"fieldname":"visitor_type",
			"label": __("Visitor Type"),
			"fieldtype": "Select",
			"options": "All\nVendor\nGuest\nCandidate\nOther",
			"default": "All"
		}	
	]
}
