// Copyright (c) 2017, Indictrans and contributors
// For license information, please see license.txt

frappe.ui.form.on('Visitor Information', {
	refresh: function(frm) {
		if (frm.doc.__islocal == undefined) {
			cur_frm.set_df_property("take_image","hidden",0)
		} else {
			cur_frm.set_df_property("take_image","hidden",1)
		}
	},
	onload: function(frm) {
		if (frm.doc.__islocal == 1) {
			cur_frm.set_value("check_in_time","")
			cur_frm.set_value("check_out_time","")
			frm.refresh_field("check_in_time");
			frm.refresh_field("check_out_time");	
		}
	},
	take_image: function(frm) {
		var me = this;
		var dialog = new frappe.ui.Dialog({
			width: 1900,
			title: "<b> Set Vistor Image </b>",
			fields:[
				{fieldtype: 'HTML',
					fieldname:'image_wrapper', label: __("Image Wrapper")},
				{fieldtype: 'Section Break',
					fieldname:'sb02'},
			]
		});
		var counter = new Date().getSeconds();
		$(dialog.body).find("[data-fieldname='image_wrapper']").html(frappe.render_template("image_template", {counter: counter}))
		dialog.show();
		(function() {
		  // The width and height of the captured photo. We will set the
		  // width to the value defined here, but the height will be
		  // calculated based on the aspect ratio of the input stream.
		  var image_data =null;
		  var width = 320;    // We will scale the photo width to this
		  var height = 0;     // This will be computed based on the input stream

		  // |streaming| indicates whether or not we're currently streaming
		  // video from the camera. Obviously, we start at false.

		  var streaming = false;

		  // The various HTML elements we need to configure or control. These
		  // will be set by the startup() function.

		  var video = null;
		  var canvas = null;
		  var photo = null;
		  var startbutton = null;

		  function startup() {
		    video = document.getElementById(String(counter)+'video');
		    canvas = document.getElementById(String(counter)+'canvas');
		    photo = document.getElementById(String(counter)+'photo');
		    startbutton = document.getElementById(String(counter)+'startbutton');
		    saveimage = document.getElementById(String(counter)+'saveimage');

		    navigator.getMedia = ( navigator.getUserMedia ||
		                           navigator.webkitGetUserMedia ||
		                           navigator.mozGetUserMedia ||
		                           navigator.msGetUserMedia);

		    navigator.getMedia(
		      {
		        video: true,
		        audio: false
		      },
		      function(stream) {
		        if (navigator.mozGetUserMedia) {
		          video.mozSrcObject = stream;
		        } else {
		          var vendorURL = window.URL || window.webkitURL;
		          video.src = vendorURL.createObjectURL(stream);
		        }
		        video.play();
		      },
		      function(err) {
		        console.log("An error occured! " + err);
		      }
		    );

		    video.addEventListener('canplay', function(ev){
		      if (!streaming) {
		        height = video.videoHeight / (video.videoWidth/width);
		      
		        // Firefox currently has a bug where the height can't be read from
		        // the video, so we will make assumptions if this happens.
		      
		        if (isNaN(height)) {
		          height = width / (4/3);
		        }
		      
		        video.setAttribute('width', width);
		        video.setAttribute('height', height);
		        canvas.setAttribute('width', width);
		        canvas.setAttribute('height', height);
		        streaming = true;
		      }
		    }, false);

		    startbutton.addEventListener('click', function(ev){
		      takepicture();
		      ev.preventDefault();
		    }, false);
		    
		    saveimage.addEventListener('click', function(ev){
		    	if (image_data) {
		    		_image = image_data.split(",")[1];
		    		create_image(_image, streaming);
		    		dialog.hide();
		    		MediaStreamTrack.stop()
		    	} else {
		    		frappe.msgprint(__("Please Take Image First"));
		    	}
		    	// ev.preventDefault();
		    }, false);
		    clearphoto();
		  }

		  // Fill the photo with an indication that none has been
		  // captured.

		  function clearphoto() {
		    var context = canvas.getContext('2d');
		    context.fillStyle = "#AAA";
		    context.fillRect(0, 0, canvas.width, canvas.height);

		    var data = canvas.toDataURL('image/png');
		    photo.setAttribute('src', data);
		  }
		  
		  // Capture a photo by fetching the current contents of the video
		  // and drawing it into a canvas, then converting that to a PNG
		  // format data URL. By drawing it on an offscreen canvas and then
		  // drawing that to the screen, we can change its size and/or apply
		  // other changes before drawing it.

		  function takepicture() {
		    var context = canvas.getContext('2d');
		    if (width && height) {
		      canvas.width = width;
		      canvas.height = height;
		      context.drawImage(video, 0, 0, width, height);
		    
		      var data = canvas.toDataURL('image/png');
		      image_data = data;
		  	    	
			// var data = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
			// window.location.href=data; 

		      photo.setAttribute('src', data);
		    } else {
		      clearphoto();
		    }
		  }

		  // Set up our event listener to run the startup process
		  // once loading is complete.
		  // window.addEventListener('load', startup, false);
		  	startup();

		})();
		
	},
	contact_no: function(frm) {
		if (frm.doc.contact_no.length != 10 || isNaN(frm.doc.contact_no)) {
			frappe.msgprint(__("Please enter 10 digit"));
			frm.doc.contact_no =  ""
			refresh_field("contact_no")			
		}
	},
	laptop: function(frm) {
		if (frm.doc.laptop == 1){
			cur_frm.toggle_reqd("serial_number", true);
		} else {
			cur_frm.toggle_reqd("serial_number", false);
		}
	},
	phones: function(frm) {
		if (frm.doc.phones == 1){
			cur_frm.toggle_reqd("mobile_information", true);
		} else {
			cur_frm.toggle_reqd("mobile_information", false);
		}
	}
});

create_image = function(_image, streaming) {
	 frappe.call({
			method: "visitor_management_system.vms.doctype.visitor_information.visitor_information.save_image_to_file",
			args: {
				"doc" :cur_frm.doc,
				"image": _image		
			},
			callback: function(r, rt) {
				if(r.message) {
					cur_frm.reload_doc();

				}
			}
		})
};