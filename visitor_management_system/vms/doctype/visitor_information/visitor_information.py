# -*- coding: utf-8 -*-
# Copyright (c) 2017, Indictrans and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt, cstr, cint
from frappe.model.document import Document
from frappe.model.naming import make_autoname
import json
from random import randrange, uniform

class VisitorInformation(Document):
	def autoname(self):
		# naming_series = 'SNV' + "-" + cstr(frappe.utils.get_datetime(frappe.utils.now()).strftime("%d-%m-%Y"))+"-"+'.#####'
		naming_series = 'SNV' + "-" + cstr(frappe.utils.get_datetime(self.date_of_visit).strftime("%d-%m-%Y"))+"-"+'.####'
		self.name = make_autoname(naming_series)
		self.visitor_no = self.name

	def validate(self):
		company_logo = frappe.db.get_value("VMS Settings", "VMS Settings", "company_logo")
		if company_logo:
			self.company_logo = company_logo 

@frappe.whitelist()
def save_image_to_file(doc, image):
	from frappe.handler import uploadfile
	args = json.loads(doc)
	frappe.form_dict['from_form'] = 1
	frappe.form_dict['doctype'] = args['doctype']
	frappe.form_dict['docname'] = args['name']
	frappe.form_dict['filename'] = cstr(randrange(0, 10000))+cstr(args['name'])
	frappe.form_dict['filedata'] = image
	attachment = uploadfile()
	if attachment.get('file_url'):
		vist_info = frappe.get_doc("Visitor Information", args['name'])
		vist_info.visitor_image = attachment.get('file_url')
		vist_info.save(ignore_permissions=True)
	return True

def get_permission_query_conditions(user):
	if not user: user = frappe.session.user
	roles = frappe.get_roles(user)
	if user == "Administrator":
		return ""
	else:
		if "Maintenance Manager" in roles:
			return """(`tabVisitor Information`.owner = '{0}')""".format(user)



















