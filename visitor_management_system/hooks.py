# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "visitor_management_system"
app_title = "VMS"
app_publisher = "Indictrans"
app_description = "Visitor Management System"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "arpit.j@indictranstech.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/visitor_management_system/css/visitor_management_system.css"
# app_include_js = "/assets/visitor_management_system/js/visitor_management_system.js"

# include js, css files in header of web template
# web_include_css = "/assets/visitor_management_system/css/visitor_management_system.css"
# web_include_js = "/assets/visitor_management_system/js/visitor_management_system.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "visitor_management_system.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "visitor_management_system.install.before_install"
# after_install = "visitor_management_system.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "visitor_management_system.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
permission_query_conditions = {
	"Visitor Information":"visitor_management_system.vms.doctype.visitor_information.visitor_information.get_permission_query_conditions"
	}
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"visitor_management_system.tasks.all"
# 	],
# 	"daily": [
# 		"visitor_management_system.tasks.daily"
# 	],
# 	"hourly": [
# 		"visitor_management_system.tasks.hourly"
# 	],
# 	"weekly": [
# 		"visitor_management_system.tasks.weekly"
# 	]
# 	"monthly": [
# 		"visitor_management_system.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "visitor_management_system.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "visitor_management_system.event.get_events"
# }